import { Component, OnInit, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Cable } from '../classes/cable';
import { modalController } from '@ionic/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-cable-amount',
  templateUrl: './cable-amount.component.html',
  styleUrls: ['./cable-amount.component.scss'],
})
export class CableAmountComponent implements OnInit {

  @Input() cable:Cable
  enteredAmount: number;
  @Input() existing: boolean;
  @Input() modal;
  @Input() ein:boolean;
  constructor(public modalController:ModalController) { }

  ngOnInit() {
    console.log(this.ein)
    if(this.ein){
      console.log(this.cable);
      this.enteredAmount = this.cable.amountTotal - this.cable.amount;
    }else {
      this.enteredAmount = this.cable.amount;
    }
    console.log(this.enteredAmount);
  }

  confirm(){
    this.cable.amount = this.enteredAmount;
    this.modalController.dismiss({cable: this.cable, existing: this.existing});
  }

}
