import { TestBed } from '@angular/core/testing';

import { ListService } from './listservice.service';

describe('ListserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListService = TestBed.get(ListService);
    expect(service).toBeTruthy();
  });
});
