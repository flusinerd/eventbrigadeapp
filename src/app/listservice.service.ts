import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { List } from './classes/list';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class ListService {
  
  selectedListSubject = new BehaviorSubject<List>(new List);
  listSubject = new BehaviorSubject<List[]>([]);
  lastList = new BehaviorSubject<List>(new List);
  

  constructor(private http:HttpClient) { }

  public getLists(){
    this.http.get(`${environment.apiUrl}/lists`).toPromise()
    .then((lists:List[]) => {
      this.listSubject.next(lists);
    });
  }

  public getList(list: List): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/lists/${list.id}`).toPromise()
  }

  public deleteList(list: List): Promise<Object>{
    return this.http.delete(`${environment.apiUrl}/lists/${list.id}`).toPromise();
  }

  public updateList(list: List): Promise<Object>{
    return this.http.put(`${environment.apiUrl}/lists/${list.id}`, list).toPromise();
  }

  public createList(list: List): Promise<Object>{
    return this.http.post(`${environment.apiUrl}/lists`, list).toPromise();
  }

  public getListById(id: string): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/lists/${id}`).toPromise();
  }
}
