import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { MatSelectModule,MatFormFieldModule, MatIconModule, MatSnackBarModule, MatDialogModule } from '@angular/material';
import { CableAmountComponent } from '../cable-amount/cable-amount.component';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatSelectModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatDialogModule,
    MatIconModule,
  ],
  declarations: [HomePage, CableAmountComponent],
  entryComponents: [
    CableAmountComponent,
  ]
})
export class HomePageModule {}
