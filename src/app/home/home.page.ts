import { Component } from "@angular/core";
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import { ListService } from '../listservice.service';
import { Router } from "@angular/router";
import { Item } from '../classes/item';
import { ItemService } from '../services/item.service';
import { List } from '../classes/list';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { Cable } from '../classes/cable';
import { CableService } from '../services/cable.service';
import { CableAmountComponent } from '../cable-amount/cable-amount.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  lists: List[] = [];
  selectedList: List;
  scannedItems: Item[] = [];
  scannedItemsIn: Item[] = [];
  scannedCablesIn: Cable[] = [];
  scannedCables: Cable[] = [];
  cableAmount: number;
  dialogRef: MatDialogRef<CableAmountComponent>;
  barcodeScannerOptions: BarcodeScannerOptions;

  constructor(private barcodeScanner: BarcodeScanner,
    private listService: ListService,
    private router: Router,
    private itemService: ItemService,
    private snackBar: MatSnackBar,
    private cableService: CableService,
    public modalController: ModalController,
  ) {
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
    this.listService.getLists();
    this.listService.listSubject.subscribe((lists: List[]) => {
      this.lists = lists;
    })
  }

  ausbuchen() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        if (!barcodeData.cancelled) {
          this.itemService.getItemByQR(barcodeData.text)
            .then((item: Item) => {
              // console.log(item);
              if (item != null) {
                item.outside = true;
                this.scannedItems.push(item);
              }
              else {
                this.cableService.getCableByQr(barcodeData.text)
                  .then(async (cable: Cable) => {
                    let existing = false;
                    for (let i = 0; i < this.scannedCables.length; i++) {
                      const element = this.scannedCables[i];
                      if (element.id === cable.id) {
                        existing = true;
                        cable.amount = this.scannedCables[i].amount;
                        break;
                      }
                    }
                    try {
                      const modal = await this.modalController.create({ component: CableAmountComponent, componentProps: { cable: cable, existing: existing } });
                      await modal.present();
                      const { data }: any = await modal.onWillDismiss();
                      if (data.existing) {
                        for (let i = 0; i < this.scannedCables.length; i++) {
                          if (this.scannedCables[i].id == data.cable.id) {
                            this.scannedCables[i] = data.cable;
                          }
                        }
                      } else {
                        this.scannedCables.push(data.cable);
                      }
                    } catch (error) {
                      console.error(error);
                    }
                  });
              }
            })
        }
      })
  }

fertig(){
  if (this.selectedList){
  this.itemService.scannedItemsSubject.next(this.scannedItems);
  this.selectedList.items = this.scannedItems;
  let promises = [];
  this.scannedCables.forEach((cable:Cable) => {
    promises.push(this.cableService.addCableToList(cable, this.selectedList, cable.amount));
  })
  this.scannedItems.forEach((item: Item) => {
    promises.push(this.itemService.updateItem(item));
  });
  Promise.all(promises)
    .then(() => {
      this.listService.updateList(this.selectedList)
        .then((list: List) => {
          this.scannedItems = [];
          this.scannedCables = [];
          this.listService.lastList.next(list);
          this.router.navigate(['liste']);
        })
    })
    .catch((err) => {
      console.error(err);
    });
  }
  else {
    this.snackBar.open("Eine Liste auswählen", null, {duration: 3000});
  }
}

einbuchen(){
  this.barcodeScanner
    .scan()
    .then(barcodeData => {
      if (!barcodeData.cancelled) {
        this.itemService.getItemByQR(barcodeData.text)
          .then((item: Item) => {
            if (item !== null) {
              item.outside = false;
              this.scannedItemsIn.push(item);
            }
            else {
              this.cableService.getCableByQr(barcodeData.text)
                  .then(async (cable: Cable) => {
                    let existing = false;
                    for (let i = 0; i < this.scannedCablesIn.length; i++) {
                      const element = this.scannedCablesIn[i];
                      if (element.id === cable.id) {
                        existing = true;
                        cable.amount = this.scannedCablesIn[i].amount;
                        break;
                      }
                    }
                    try {
                      const modal = await this.modalController.create({ component: CableAmountComponent, componentProps: { cable: cable, existing: existing, ein: true } });
                      await modal.present();
                      const { data }: any = await modal.onWillDismiss();
                      if (data.existing) {
                        for (let i = 0; i < this.scannedCablesIn.length; i++) {
                          if (this.scannedCablesIn[i].id == data.cable.id) {
                            this.scannedCablesIn[i] = data.cable;
                          }
                        }
                      } else {
                        this.scannedCablesIn.push(data.cable);
                      }
                    } catch (error) {
                      console.error(error);
                    }
                  });
            }
          })
      }
    })
    .catch(err => {
      alert("Error " + err)
    })
}

changeCableAmount(amount: number){
  this.cableAmount = amount;
}

fertigIn(){
  this.itemService.scannedItemsInSubject.next(this.scannedItemsIn);
  let promises = [];
  this.scannedItemsIn.forEach((item: Item) => {
    console.log(item)
    promises.push(this.itemService.updateItem(item));
  });
  this.scannedCablesIn.forEach((cable:Cable) => {
    promises.push(this.cableService.cableBack(cable, cable.amount));
  })
  Promise.all(promises)
    .then(() => {
      this.snackBar.open("Alles erfolgreich eingebucht", null, { duration: 3000 });
    })
    .catch((err) => {
      console.error(err);
    }).finally(() => {
      this.scannedItemsIn = [];
      this.scannedItems = [];
    });
  
}

}