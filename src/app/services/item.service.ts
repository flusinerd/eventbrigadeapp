import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { Item } from '../classes/item';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  public scannedItemsSubject = new BehaviorSubject<Item[]>([]);
  public scannedItemsInSubject = new BehaviorSubject<Item[]>([]);


  constructor(private http:HttpClient) { }

  public getItemByQR(qrcode:string): Promise<Object>{
    return this.http.get(`${environment.apiUrl}/items/qrcode/${qrcode}`).toPromise();
  }

  public updateItem(item:Item){
    return this.http.put(`${environment.apiUrl}/items/${item.id}`, item).toPromise();
  }
}
