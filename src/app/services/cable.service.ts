import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Cable } from '../classes/cable';
import { List } from '../classes/list';

@Injectable({
  providedIn: 'root'
})
export class CableService {

  constructor(private http: HttpClient) { }

  public getCableByQr(qrcode: string):Promise<Object>{
    return this.http.get(`${environment.apiUrl}/cables/qrcode/${qrcode}`).toPromise();
  }

  public addCableToList(cable: Cable, list:List, amount:number): Promise<Object>{
    return this.http.post(`${environment.apiUrl}/cables/link`, {cable: cable, list: list, amount: amount }).toPromise();
  }

  public cableBack(cable: Cable, amount: number): Promise<Object>{
    return this.http.post(`${environment.apiUrl}/cables/link/back`, {cable: cable, amount: amount}).toPromise();
  }
}
