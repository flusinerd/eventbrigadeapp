import { Component, OnInit, ChangeDetectorRef, AfterViewInit, OnChanges } from '@angular/core';
import { ListService } from '../listservice.service';
import { List } from '../classes/list';
import { MatSnackBar, MatTableDataSource } from '@angular/material';
import { Item } from '../classes/item';
import { Router } from '@angular/router';
import { File } from '@ionic-native/file/ngx';

import 'jspdf';
import 'jspdf-autotable';
import { FileOpener } from '@ionic-native/file-opener/ngx';
declare let jsPDF;

@Component({
  selector: 'app-liste',
  templateUrl: './liste.page.html',
  styleUrls: ['./liste.page.scss'],
})
export class ListePage {

  list: MatTableDataSource<any> = new MatTableDataSource([]);
  selectedList: List;

  columnsToDisplay = ['name', 'amountTotal', 'actions'];

  constructor(private listService:ListService, private snackBar:MatSnackBar, private router:Router, private file:File, private fileOpener: FileOpener) {
    this.listService.lastList.subscribe((list:List) => {this.selectedList = list;});
    this.router.events.subscribe(() => {
      this.getList();
    })
  }

  getList(){
    this.listService.getListById("1")
    .then((list:List) => {
      list.cables.forEach(cable => {
        cable.name = cable.cable.name;
        cable.amount = cable.cable.amount;
        cable.qrcode = cable.cable.qrcode;
        cable.comment = cable.cable.comment;
      })
      let data = list.items.concat(list.cables)
      this.list.data = data;
    })
    .catch((err) => {
      console.error(err);
    })
  }

  public print() {
    let doc = new jsPDF();
    let header = function(data) {
      doc.setFontSize(18);
      doc.setTextColor(40);
      doc.setFontStyle('normal');
      doc.text("Liste", data.settings.margin.left, 20);
    }
    let columns = [
      {
        title: "Name",
        dataKey: 'name'
      },
      {
        title: "Kommentar", 
        dataKey: 'comment'
      },
      {
        title: "Anzahl",
        dataKey: 'amount'
      }
    ]
    let rows = this.list.data
    rows.forEach((row) => {
      row.amount = "-";
    })
    doc.autoTable(columns, rows, { margin: {top: 30}, beforePageContent: header});
    let pdfOut = doc.output();
    let buffer = new ArrayBuffer(pdfOut.length);
    let array = new Uint8Array(buffer);
    for (var i = 0; i < pdfOut.length; i++){
      array[i] = pdfOut.charCodeAt(i);
    }

    const directory = this.file.externalApplicationStorageDirectory;


    const fileName= Math.floor(Math.random() * 5000000) + ".pdf";

    this.file.writeFile(directory, fileName, buffer)
    .then((success) => {
      this.fileOpener.open(directory+'/'+fileName, 'application/pdf')
    })
    .catch((err) => {
      console.error(err);
    })
  }

  // public removeItemFromList(item: Item){
  //   this.list.data.items = this.arrayRemove(this.list.items, item);
  //   this.listService.updateList(this.list)
  //   .then((list:List) => {
  //     this.list = list;
  //     this.snackBar.open("Aus Liste entfernt", null, {duration: 3000});
  //   })
  //   .catch((err) =>{
  //     console.error(err);
  //   });
  // }

  // private arrayRemove(arr:Item[], value:Item){
  //   return arr.filter(function (element) {
  //     return element.id != value.id;
  //   });
  // }

}
