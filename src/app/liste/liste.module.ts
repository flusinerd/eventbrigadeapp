import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListePage } from './liste.page';
import { MatTableModule, MatSnackBar, MatSnackBarModule, MatIconModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: ListePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatTableModule,
    MatSnackBarModule,
    MatIconModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ListePage]
})
export class ListePageModule {}
