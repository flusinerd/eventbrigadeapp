import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewListPage } from './new-list.page';
import { MatFormFieldModule, MatSlideToggleModule, MatInputModule, MatSnackBarModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: NewListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    MatInputModule,
    MatSnackBarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewListPage]
})
export class NewListPageModule {}
