import { Component, OnInit } from '@angular/core';
import { List } from '../classes/list';
import { ListService } from '../listservice.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-list',
  templateUrl: './new-list.page.html',
  styleUrls: ['./new-list.page.scss'],
})
export class NewListPage implements OnInit {

  list = new List;

  constructor(private listService:ListService, private snackBar:MatSnackBar, private router:Router) { }

  ngOnInit() {
  }

  public createList() {
    this.listService.createList(this.list)
    .then(() => {
      this.snackBar.open('Erfolgreich erstellt', null, {duration: 3000});
      this.router.navigateByUrl("/home");
      this.listService.getLists();
    })
    .catch((err) => {
      console.error(err);
    })
  }

}
